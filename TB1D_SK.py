import numpy as np
import matplotlib.pylab as plt
import time

class TB1D_SK():
    
    def __init__(self, n, delta_x, delta_y, Vss, Vsp, Vpp_s, Vpp_p, a, a_x, a_y, R0, plotRes):
        
        self.n = n
        self.a = a
        self.a_x = a_x
        self.a_y = a_y
        self.R0 = R0
        
        #Slater-Koster
        #(a_x, a_y)
        #Ess = Vss
        #Esx = a_x*Vsp
        #Esy = a_y*Vsp
        #Exx = (a_x**2)*Vpp_s+(1-a_x**2)*Vpp_p
        #Eyy = (a_y**2)*Vpp_s+(1-a_y**2)*Vpp_p 
        #Exy = a_x*a_y*Vpp_s - a_x*a_y*Vpp_p
        
        self.t_ndiag = np.array([[Vss, a_x*Vsp, a_y*Vsp], 
                     [a_x*Vsp, (a_x**2)*Vpp_s+(1-a_x**2)*Vpp_p, a_x*a_y*Vpp_s - a_x*a_y*Vpp_p],
                     [a_y*Vsp, a_x*a_y*Vpp_s - a_x*a_y*Vpp_p,(a_y**2)*Vpp_s+(1-a_y**2)*Vpp_p]])
        
        self.t_diag = np.array([[0, 0, 0,],[0, delta_x, 0],[0, 0, delta_y]])
        
        
        self.H = np.zeros((3*self.n,3*self.n))
        self.plotRes = plotRes
        
        
        self.initR()
        self.initH()
        self.getES()
        self.plotES()

        print("Hamiltonian (s, px, py)")
        for i in range (3*self.n):
            for j in range (3*self.n):
                 print(self.H[i][j], end=' ')
            print('\n', end ='')
        
        print('\n')
        print("Eigenvalues \t \t Eigenvectors")
        for i in range(3*self.n):
            row = self.psi[:,i]
            fmt = '{0:.4f}' if self.energies[i] >= 0 else '{0:.3f}'
            print(fmt.format(self.energies[i]), "\t \t", '  '.join("{0:.3f}".format(item) for item in row))
             
    def initR(self):
        self.R = np.linspace(0, (self.n-1)*self.a, self.n)
        
    def initH(self):
        coord = np.zeros((self.n,self.n))
        coord +=np.diag(np.ones(self.n-1), +1)
        coord +=np.diag(np.ones(self.n-1), -1)
        
        for i in range (self.n):
            for j in range (self.n):
                if(i == j):
                    for x in range (3):
                        for y in range (3):
                            self.H[3*i+x][3*j+y] = self.t_diag[x][y]
                               
                if(coord[i][j] == 1):
                    for x in range (3):
                        for y in range (3):
                            self.H[3*i+x][3*j+y] = self.t_ndiag[x][y]
                    
 

    def getES(self):
        
        energies, psi = np.linalg.eig(self.H)
        
        idx = np.argsort(energies)
        
        self.energies = energies[idx]
        self.psi = psi[:,idx]
        
        
    def getPsiR(self, i, x, y):
        
        def phi(x,y):
            return np.exp(-np.sqrt(x**2 + y**2)/self.R0)
        
        def phi_x(x,y):
            return np.exp(-np.sqrt(x**2 + y**2)/self.R0)*x
        
        def phi_y(x,y):
            return np.exp(-np.sqrt(x**2 + y**2)/self.R0)*y
        
        
        psiR = 0.
        
        for n in range(self.n):
            for m in range (3):
                if(m == 0):
                    psiR += self.psi[3*n, i] * phi(x-self.R[n],y)
                elif (m == 1):
                    psiR += self.psi[3*n+1, i] * phi_x(x-self.R[n],y)
                elif (m == 2):
                    psiR += self.psi[3*n+2, i] * phi_y(x-self.R[n],y)
                      
        return psiR
    
        
    def plotES(self):
        
        plt.figure(figsize=(7,7))
        
        grid = plt.GridSpec(nrows=3*self.n, ncols=8)
        
        # spectrum
        plt.subplot(grid[0:3*self.n, 0])
        plt.title('energy levels')
        for i in range(3*self.n):
            plt.plot([0,1], 
                     [self.energies[i], self.energies[i]])
        plt.xticks([])
        plt.gca().invert_yaxis()
        plt.ylabel('energies (eV)')
        
        # wave functions
        x = np.linspace(np.min(self.R)-1, np.max(self.R)+1, self.plotRes)
        y = np.linspace(-1, 1, self.plotRes)
        xGrid, yGrid = np.meshgrid(x, y)
        for i in range(3*self.n):
            
            
            plt.subplot(grid[i, 1:4])
            plt.xticks([])
            plt.yticks([])
            if(i == 0):
                plt.title('charge density')

            for j, r in enumerate(self.R):
                
                plt.plot(r, 0, 
                         marker='.', markersize=10,
                         color='tab:green')
                
                z = np.abs(self.getPsiR(i, xGrid, yGrid))**2
                
                plt.pcolor(x, y, z, 
                       cmap='Reds')
                
                plt.axis('equal')
            
            plt.subplot(grid[i, 5:8])
            plt.xticks([])
            plt.yticks([])
            if(i == 0):
                plt.title('wavefunctions')

            for j, r in enumerate(self.R):
                
                plt.plot(r, 0, 
                         marker='.', markersize=10,
                         color='tab:green')
                
                z = self.getPsiR(i, xGrid, yGrid)
                
                plt.pcolor(x, y, z, 
                       cmap='coolwarm')
                
                plt.axis('equal')
                     
        plt.show()