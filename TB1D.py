import numpy as np
import matplotlib.pylab as plt
import time

class TB1D():
    
    def __init__(self, n, t, a, plotRes):
        
        self.n = n
        self.t = t
        self.a = a
        
        self.H = np.zeros((n,n))
        self.plotRes = plotRes
        
        t = time.time()
        
        self.initR()
        self.initH()
        self.getES()
        self.plotES()
        
        print(t - time.time())
        
    def initR(self):
        self.R = np.linspace(0, (self.n-1)*self.a, self.n)
        
    def initH(self):
        
        self.H += np.diag(self.t * np.ones(self.n-1), +1)
        self.H += np.diag(self.t * np.ones(self.n-1), -1)
        
    def getES(self):
        
        energies, psi = np.linalg.eig(self.H)
        
        idx = np.argsort(energies)
        
        self.energies = energies[idx]
        self.psi = psi[:,idx]
        
    def getPsiR(self, i, x, y):
        
        def phi(x,y):
            return np.exp(-(x**2 + y**2))
        
        psiR = phi(x,y) * 0.
        
        for n in range(self.n):
            psiR += self.psi[n, i] * phi(x-self.R[n],y)
        
        return psiR
        
    def plotES(self):
        
        plt.figure(1, figsize=(9,7))
        
        grid = plt.GridSpec(nrows=self.n, ncols=4)
        
        # spectrum
        plt.subplot(grid[0:self.n, 0])
        plt.title('energy levels')
        for i in range(self.n):
            plt.plot([0,1], 
                     [self.energies[i], self.energies[i]])
        plt.xticks([])
        plt.ylabel('energies (eV)')
        
        # wave functions
        x = np.linspace(np.min(self.R)-1, np.max(self.R)+1, self.plotRes)
        y = np.linspace(-0.5, +0.5, self.plotRes)
        xGrid, yGrid = np.meshgrid(x, y)
        for i in range(self.n):
            
            plt.subplot(grid[i, 1:4])
            plt.xticks([])
            plt.yticks([])
            if(i == 0):
                plt.title('wavefunctions')

            for j, r in enumerate(self.R):
                
                plt.plot(r, 0, 
                         marker='.', markersize=15,
                         color='tab:green')
                
                z = np.abs(self.getPsiR(i, xGrid, yGrid))**2.0
                
                plt.pcolor(x, y, z, 
                       cmap='Reds', 
                       vmin=z.min(), 
                       vmax=z.max())
                
                plt.axis('equal')
        
        plt.show()